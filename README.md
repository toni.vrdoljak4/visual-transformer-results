# Label models

Since label problem of the LAD dataset is a regular multi-class classification problem cross entrpy loss will be used. This is implemented by PyTorch class ```torch.nn.CrossEntropyLoss```.
Accuracy metric will be used to evaluate the quality of models and how well they generalise.

## Experiment 1: Comparing models with and without bounding box cropping

LAD dataset contains coordinates for bounding boxes that contain relevant objects. The first experiment we perform is to compare model that was trained on the full image and the one that was trained on the bounding box.

The configuration of both models is the following:

```yaml
batch_size: 80
model_arch: b32
num_classes: 230
image_size: 384
dropout_rate: 0.1

optimizer:
  type: SGD
  parameters:
    lr: 0.03
    weight_decay: 0.0
    momentum: 0.9

lr_scheduler:
  type: OneCycleLR
  step_per_batch: True
  parameters:
    max_lr: 0.03
    pct_start: 0.05
    total_steps: 20000
```

<figure>
<img src="images/labels/experiment1/legend.png"/>
<img src="images/labels/experiment1/acc1_valid.svg"/>
<figcaption>Comparison of validation accuracy with and without cropping</figcaption>
</figure>


From the above plot it can be seen that validation accuracy is larger (it generalises better) for the model without cropping. One possible explanation for this is that since the cropped image contains less detail and therefore less variation, overfitting is more likely to occur in the case of the model with cropping (the model doesn't generalise well). This can be seen in the loss train and validation plots below.


<figure>
<img src="images/labels/experiment1/legend.png"/>
<img src="images/labels/experiment1/loss_train.svg"/>
<figcaption>Comparison of training loss with and without cropping</figcaption>
</figure>

<figure>
<img src="images/labels/experiment1/legend.png"/>
<img src="images/labels/experiment1/loss_valid.svg"/>
<figcaption>Comparison of validation loss with and without cropping</figcaption>
</figure>



For the model with cropping the validation loss starts increasing at one point while the training loss continues to decrease. This is a sign of overfitting. There is no significant increase of validation loss in the case of model without cropping.

Based on this in this conclusion we don't use cropping in the following experiments.



## Experiment 2: Comparing ViT B16-224 and B32 architectures

Next we compare performance of B16-224 (input image of size 224x224) and B32 ViT models.


From training loss, validation loss and validation accuracy plots it can be seen that B32 model slightly outperforms B16-224 model. This is expected since B32 architecture has more parameters (the patch embedding tensor has 4 times more parameters).


<figure>
<img src="images/labels/experiment2/legend.png"/>
<img src="images/labels/experiment2/loss_train.svg"/>
<figcaption>Comparison of training loss for B32 and B16-224 architectures</figcaption>
</figure>

<figure>
<img src="images/labels/experiment2/legend.png"/>
<img src="images/labels/experiment2/loss_valid.svg"/>
<figcaption>Comparison of validation loss for B32 and B16-224 architectures</figcaption>
</figure>

<figure>
<img src="images/labels/experiment2/legend.png"/>
<img src="images/labels/experiment2/acc1_valid.svg"/>
<figcaption>Comparison of validation accuracy for B32 and B16-224 architectures</figcaption>
</figure>

In the following experiments we use B32 architecture.

## Experiment 3: Learning rates

In this experiment we compare performance for 3 different learning rates. Values used are 0.003, 0.03 and 0.3.


<figure>
<img src="images/labels/experiment3/all_legend.png"/>
<img src="images/labels/experiment3/all.svg"/>
<figcaption>Comparison of validation accuracy for different learning rates</figcaption>
</figure>


From the previous plot it can be seen that the case of lr=0.3 has poor accuracy on the validation set. The same is true for validation loss. To save time we stopped the training early. 


For greater clarity, we give below only the plots for learning rates 0.003 and 0.03. The learning rate 0.3 is left out.

<figure>
<img src="images/labels/experiment3/legend.png"/>
<img src="images/labels/experiment3/loss_train.svg"/>
<figcaption>Comparison of training loss for different learning rates</figcaption>
</figure>

<figure>
<img src="images/labels/experiment3/legend.png"/>
<img src="images/labels/experiment3/loss_valid.svg"/>
<figcaption>Comparison of validation loss for different learning rates</figcaption>
</figure>


From the plot below it can be seen that the model that was trained with learning rate 0.03 generalises better than the rest.

<figure>
<img src="images/labels/experiment3/legend.png"/>
<img src="images/labels/experiment3/acc1_valid.svg"/>
<figcaption>Comparison of validation accuracy for different learning rates</figcaption>
</figure>


## Experiment 4: Data augmentation by a random small degree rotation

We use PyTorches built in transform  ```torchvision.transforms.RandomRotation``` with ```degrees=15```. The idea is that small rotation should not change the meaning and therefore the label of the image. The idea is taken from the baseline method for the LAD dataset and can be seen [here](https://github.com/AIChallenger/AI_Challenger_2018/blob/master/Baselines/zero_shot_learning_baseline/train_CNN.py#L93).

<figure>
<img src="images/labels/experiment4/legend.png"/>
<img src="images/labels/experiment4/loss_train.svg"/>
<figcaption>Comparison of training loss with and without random rotation</figcaption>
</figure>

<figure>
<img src="images/labels/experiment4/legend.png"/>
<img src="images/labels/experiment4/loss_valid.svg"/>
<figcaption>Comparison of validation loss with and without random rotation</figcaption>
</figure>


<figure>
<img src="images/labels/experiment4/legend.png"/>
<img src="images/labels/experiment4/acc1_valid.svg"/>
<figcaption>Comparison of validation accuracy with and without random rotation</figcaption>
</figure>

Note: data augmentation with random horizontal flip is used by default in all experiments.

## Evaluation of best model on test set

In this section we take the model that had the best performance on the validation set and we evaluate it on the test set.

The model with best performance on validation set is the B32 model with learning rate 0.03 and no bounding box cropping.

On the test set it yields an accuracy of 91.7602%

![](images/labels/evaluation/evaluation.png)


Macro F1 is low, 0.2602 since dataset is imbalanced. Additionally we calculated micro F1 to be 0.9176.


We also tried training with ```weight``` parameter of the ```torch.nn.CrossEntropyLoss``` set to the inverse count of each class. Another approach was to use ```torch.utils.data.WeightedRandomSampler``` in the dataloader with ```weights``` parameter of the sampler also set to the inverse count of each class. None of the approaches increased macro F1 significantly.

In the plot below we can see that the confusion matrix is mostly diagonal:

![](images/labels/evaluation/conf_mat_full.png)


For greater clarity, we also give a zoomed in part of the confusion matrix.

![](images/labels/evaluation/conf_mat_small.png)


Even though they are rare, off-diagonal elements are clearly visible in the plot above. For classes with small number of examples that have a few misclassified examples (off-diagonal elements) F1 score will be small. Since macro F1 is calculated as average F1 score over all classes, all classes will contribute equally to macro F1 score, regardless of their size. This causes macro F1 score to be small. Classes with small F1 score are visible on the confusion matrix diagonal as black or dark purple colors. 


# Attribute models

Attribute problem of the LAD dataset is a multi-label multi-class classification problem so binary cross entropy loss will be used. This is implemented by the PyTorch class ```torch.nn.BCEWithLogitsLoss```. The metrics that will be used to evaluate models are Jaccard index and multi-label F1 score described [here](https://en.wikipedia.org/wiki/Multi-label_classification#Statistics_and_evaluation_metrics).

## Experiment 1: Binary cross entropy with positive example weights

The first model was trained with regular binary cross entropy loss. The configuration of the model was the following:

```yaml
batch_size: 60
model_arch: b32
num_classes: 356
image_size: 384
dropout_rate: 0.1

optimizer:
  type: SGD
  parameters:
    lr: 0.03
    weight_decay: 0.0
    momentum: 0.9

lr_scheduler:
  type: OneCycleLR
  step_per_batch: True
  parameters:
    max_lr: 0.03
    pct_start: 0.05
    total_steps: 20000
```

It showed relatively poor performance in the Jaccard index metric. This was due to very sparse attributes target vectors. Most of attributes in the target vectors are 0, only a few are 1. This caused the model to achieve low training loss by mostly predicting 0, but this did not generalize well with respect to the Jaccard index. One possible solution was to put more weight on the positive examples. This is were the ```pos_weight``` parameter of the ```torch.nn.BCEWithLogitsLoss``` comes in.


Jaccard index plots for ```pos_weight=10``` and ```pos_weight=30``` are shown below alongside the plot with no positive example weighting (equivalent to ```pos_weight=1```).


<figure>
<img src="images/attributes/experiment1/legend.png"/>
<img src="images/attributes/experiment1/jaccard_valid.svg"/>
<figcaption>Comparison of validation Jaccard index for different values of pos_weight parameter</figcaption>
</figure>

From the plot it can be seen that best performance on validation set is achieved for ```pos_weight=10```.


For completeness loss plots for training and validation sets are given below. Hint: loss values are not comparable for different ```pos_weight``` values because of different weight factors.

<figure>
<img src="images/attributes/experiment1/legend.png"/>
<img src="images/attributes/experiment1/loss_train.svg"/>
<figcaption>Comparison of training loss for different values of pos_weight parameter</figcaption>
</figure>

<figure>
<img src="images/attributes/experiment1/legend.png"/>
<img src="images/attributes/experiment1/loss_valid.svg"/>
<figcaption>Comparison of validation loss for different values of pos_weight parameter</figcaption>
</figure>


In further experiments the value of ```pos_weight=10``` will be used.


## Experiment 2: Learning rates

In this experiment we compare performance for 3 different learning rates. Values used are 0.003, 0.03 and 0.3.


The plots below show training and validation loss for these learning rates.

<figure>
<img src="images/attributes/experiment2/legend.png"/>
<img src="images/attributes/experiment2/loss_train.svg"/>
<figcaption>Comparison of training loss for different learning rates</figcaption>
</figure>

<figure>
<img src="images/attributes/experiment2/legend.png"/>
<img src="images/attributes/experiment2/loss_valid.svg"/>
<figcaption>Comparison of validation loss for different learning rates</figcaption>
</figure>

<figure>
<img src="images/attributes/experiment2/legend.png"/>
<img src="images/attributes/experiment2/jaccard_valid.svg"/>
<figcaption>Comparison of validation Jaccard index for different learning rates</figcaption>
</figure>

From the plots above it can be seen that the model that was trained with learning rate 0.3 achieves the best results on both training and validation datasets.

## Experiment 3: Transfer learning (pre-training)

In this experiment we train on attributes (multi-label) problem with the model that was already pre-trained on label (standard multi-class) problem. We chose the model that showed the best performance on Experiment 3 in Label models section, i.e. the one obtained with the learning rate set to 0.03.


The plots for this experiment are shown below alongside the plot for the best model from Experiment 1, for comparison.

<figure>
<img src="images/attributes/experiment3/legend.png"/>
<img src="images/attributes/experiment3/jaccard_valid.svg"/>
<figcaption>Comparison of validation Jaccard index for pre-trained model and model without pre-training</figcaption>
</figure>

<figure>
<img src="images/attributes/experiment3/legend.png"/>
<img src="images/attributes/experiment3/loss_train.svg"/>
<figcaption>Comparison of training loss for pre-trained model and model without pre-training</figcaption>
</figure>

<figure>
<img src="images/attributes/experiment3/legend.png"/>
<img src="images/attributes/experiment3/loss_valid.svg"/>
<figcaption>Comparison of validation loss for pre-trained model and model without pre-training</figcaption>
</figure>

From the Jaccard index validation plot it can be seen that there is a significant increase of approximately 2%.


## Experiment 4: Comparison of different learning rate schedulers

Three different learning rate scheduler are compared, ```torch.optim.lr_scheduler.OneCycleLR```, ```torch.optim.lr_scheduler.CyclicLR``` and ```torch.optim.lr_scheduler.ExponentialLR```.
Their parameters are listed below. 

```yaml
lr_scheduler:
  type: OneCycleLR
  step_per_batch: True
  parameters:
    max_lr: 0.03
    pct_start: 0.05
    total_steps: 20000
```

```yaml
lr_scheduler:
  type: CyclicLR
  step_per_batch: True
  parameters:
    base_lr: 0.01
    max_lr: 0.3
```

```yaml
lr_scheduler:
  type: ExponentialLR
  step_per_batch: False
  parameters: 0.93  
```

All runs use the same optimizer ```torch.optim.SGD```.


From the Jaccard index validation plot below it can be seen that OneCycleLR slightly outperformes the rest.

<figure>
<img src="images/attributes/experiment4/legend.png"/>
<img src="images/attributes/experiment4/jaccard_valid.svg"/>
<figcaption>Comparison of validation Jaccard index for for different learning rate schedulers</figcaption>
</figure>

For completeness loss plots for training and validation sets are given below.

<figure>
<img src="images/attributes/experiment4/legend.png"/>
<img src="images/attributes/experiment4/loss_train.svg"/>
<figcaption>Comparison of training loss for different learning rate schedulers</figcaption>
</figure>

<figure>
<img src="images/attributes/experiment4/legend.png"/>
<img src="images/attributes/experiment4/loss_valid.svg"/>
<figcaption>Comparison of validation loss for different learning rate schedulers</figcaption>
</figure>



## Evaluation of best model on test set

In this section we take the model that had the best performance on the validation set and we evaluate it on the test set.

The model with the best performance is the pre-trained model from Experiment 3. It was trained with learning rate 0.3 and it has B32 architecture.

On the test set it yields a Jaccard index of 0.9016 (90.16%) and F1 score of 0.946 (94.6%).

![](images/attributes/evaluation/evaluation.png)

